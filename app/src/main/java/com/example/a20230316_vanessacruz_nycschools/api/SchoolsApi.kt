package com.example.a20230316_vanessacruz_nycschools.api

import com.example.a20230316_vanessacruz_nycschools.model.HighSchool
import retrofit2.http.GET

interface SchoolsApi {

    @GET("s3k6-pzi2.json")
    suspend fun fetchHighSchools(): List<HighSchool>

}
