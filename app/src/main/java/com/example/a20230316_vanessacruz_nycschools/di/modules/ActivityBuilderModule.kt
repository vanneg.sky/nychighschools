package com.example.a20230316_vanessacruz_nycschools.di.modules

import com.example.a20230316_vanessacruz_nycschools.MainActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class ActivityBuilderModule {

    @ContributesAndroidInjector(
        modules = [
            FragmentBuilderModule::class,
            ViewModelFactoryModule::class,
        ]
    )

    abstract fun contributeMainActivity(): MainActivity
}