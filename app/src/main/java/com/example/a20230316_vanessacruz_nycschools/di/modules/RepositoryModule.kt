package com.example.a20230316_vanessacruz_nycschools.di.modules

import com.example.a20230316_vanessacruz_nycschools.api.SATApi
import com.example.a20230316_vanessacruz_nycschools.api.SchoolsApi
import com.example.a20230316_vanessacruz_nycschools.repository.HighSchoolsRepository
import com.example.a20230316_vanessacruz_nycschools.repository.HighSchoolsRepositoryContract
import com.example.a20230316_vanessacruz_nycschools.repository.SATRepository
import com.example.a20230316_vanessacruz_nycschools.repository.SATRepositoryContract
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class RepositoryModule {
    @Provides
    @Singleton
    fun provideHighSchoolsRepository(schoolsApi: SchoolsApi): HighSchoolsRepositoryContract =
        HighSchoolsRepository(schoolsApi)


    @Provides
    @Singleton
    fun provideSATRepository(satApi: SATApi): SATRepositoryContract =
        SATRepository(satApi)
}