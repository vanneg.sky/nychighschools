package com.example.a20230316_vanessacruz_nycschools.repository

import com.example.a20230316_vanessacruz_nycschools.api.SchoolsApi
import com.example.a20230316_vanessacruz_nycschools.model.HighSchool
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import timber.log.Timber

interface HighSchoolsRepositoryContract {

    suspend fun fetchHighSchoolsList(): List<HighSchool>
}

class HighSchoolsRepository(
    private val schoolsApi: SchoolsApi,
    private val dispatcher: CoroutineDispatcher = Dispatchers.IO,
) : HighSchoolsRepositoryContract {

    override suspend fun fetchHighSchoolsList(): List<HighSchool> =
        withContext(dispatcher) {
            try {
                val schoolList = schoolsApi.fetchHighSchools()
                return@withContext schoolList
            } catch (e: Throwable) {
                e.printStackTrace()
                Timber.e("Schools repo launch an exception ${e.message}")
                throw e
            }
        }
}
