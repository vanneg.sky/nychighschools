package com.example.a20230316_vanessacruz_nycschools.ui.schools

import com.example.a20230316_vanessacruz_nycschools.model.HighSchool

sealed class SchoolsState {
    object None : SchoolsState()
    object Loading : SchoolsState()
    data class Error(val message: String? = null) : SchoolsState()
    data class Success(val highSchools: List<HighSchool>) : SchoolsState()
    object SendToDetails : SchoolsState()
}

sealed class SchoolsListIntent {
    object Init : SchoolsListIntent()
    object ClearState : SchoolsListIntent()
    data class SchoolTapped(val school: HighSchool) : SchoolsListIntent()
}