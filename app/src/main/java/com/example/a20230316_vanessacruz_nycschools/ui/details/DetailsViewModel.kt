package com.example.a20230316_vanessacruz_nycschools.ui.details

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.a20230316_vanessacruz_nycschools.model.HighSchool
import com.example.a20230316_vanessacruz_nycschools.model.HighSchoolAndSAT
import com.example.a20230316_vanessacruz_nycschools.model.NetworkResponse
import com.example.a20230316_vanessacruz_nycschools.repository.SATRepositoryContract
import kotlinx.coroutines.async
import timber.log.Timber
import javax.inject.Inject

class DetailsViewModel @Inject constructor(
    internal var satRepository: SATRepositoryContract,
) : ViewModel() {

    private val _schoolData = MutableLiveData<NetworkResponse<HighSchoolAndSAT>>()
    val schoolData: LiveData<NetworkResponse<HighSchoolAndSAT>> get() = _schoolData

    fun fetchSchoolAndScores() {
        _schoolData.postValue(NetworkResponse.Loading())
        viewModelScope.async {
            satRepository.selectedSchool?.let {
                fetchScores(it)
            }
                ?: _schoolData.postValue(NetworkResponse.Error(IllegalArgumentException("No school found")))
            Timber.e("DetailsViewModel fetchSchoolAndScore() failed")
        }
    }

    private suspend fun fetchScores(school: HighSchool) {
        try {
            val score = satRepository.fetchSATResults(school.dbn!!)
            when {
                score == null -> _schoolData.postValue(
                    NetworkResponse.Error(
                        java.lang.IllegalArgumentException("No scores found"),
                        HighSchoolAndSAT(school, null)
                    )
                )
                else -> _schoolData.postValue(
                    NetworkResponse.Success(HighSchoolAndSAT(school, score))
                )
            }
        } catch (e: Throwable) {
            Timber.e("DetailsViewModel fetchScore() failed")
            _schoolData.postValue(NetworkResponse.Error(e))
        }
    }
}