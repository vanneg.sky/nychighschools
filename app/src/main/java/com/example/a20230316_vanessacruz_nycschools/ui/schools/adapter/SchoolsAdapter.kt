package com.example.a20230316_vanessacruz_nycschools.ui.schools.adapter

import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.a20230316_vanessacruz_nycschools.model.HighSchool

class SchoolsAdapter(val schoolSelectedListener: (HighSchool) -> Unit) :
    RecyclerView.Adapter<SchoolViewHolder>() {

    private val highSchools = mutableListOf<HighSchool>()

    fun updateItems(newItems: List<HighSchool>) {
        highSchools.clear()
        highSchools.addAll(newItems)
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
        SchoolViewHolder(parent) { position -> schoolSelectedListener(highSchools[position]) }

    override fun getItemCount() = highSchools.size

    override fun onBindViewHolder(holder: SchoolViewHolder, position: Int) {
        holder.bindModelToView(highSchools[position])
    }
}