package com.example.a20230316_vanessacruz_nycschools.ui.details

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.lifecycleScope
import com.example.a20230316_vanessacruz_nycschools.R
import com.example.a20230316_vanessacruz_nycschools.databinding.FragmentDetailsBinding
import com.example.a20230316_vanessacruz_nycschools.model.HighSchool
import com.example.a20230316_vanessacruz_nycschools.model.HighSchoolAndSAT
import com.example.a20230316_vanessacruz_nycschools.model.HighSchoolSATResults
import com.example.a20230316_vanessacruz_nycschools.model.NetworkResponse
import com.google.android.material.snackbar.Snackbar
import dagger.android.support.DaggerFragment
import kotlinx.coroutines.launch
import javax.inject.Inject

class DetailsFragment : DaggerFragment() {
    private var _binding: FragmentDetailsBinding? = null
    private val binding get() = _binding!!

    @Inject
    internal lateinit var viewModelFactory: ViewModelProvider.Factory
    private val detailsViewModel: DetailsViewModel by lazy {
        ViewModelProvider(requireActivity(), viewModelFactory)[DetailsViewModel::class.java]
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View {
        _binding = FragmentDetailsBinding.inflate(inflater, container, false)
        val view = binding.root
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        observeViewModel()
        detailsViewModel.fetchSchoolAndScores()
    }

    private fun observeViewModel() {
        lifecycleScope.launch {
            detailsViewModel.schoolData.observe(viewLifecycleOwner) {
                when (it) {
                    is NetworkResponse.Loading -> binding.progressBar.visibility = View.VISIBLE
                    is NetworkResponse.Error -> handleError(it)
                    is NetworkResponse.Success -> handleSuccess(it)
                }
            }
        }
    }

    private fun handleSuccess(success: NetworkResponse.Success<HighSchoolAndSAT>) {
        binding.progressBar.visibility = View.GONE
        binding.highschoolName.text = success.data.school.schoolName
        binding.highschoolDetails.text = success.data.school.overviewParagraph
        binding.mathSatScore.text =
            getString(R.string.avg_math_score, success.data.score?.averageMathScore)
        binding.readingSatScore.text =
            getString(R.string.avg_reading_score, success.data.score?.averageCriticalReadingScore)
        binding.writingSatScore.text =
            getString(R.string.avg_writing_score, success.data.score?.averageWritingScore)
        binding.academicOp.text = "Languages"
        binding.academicOpDetails.text =
            success.data.school.languageClasses
    }

    private fun handleError(error: NetworkResponse.Error<HighSchoolAndSAT>) {
        binding.progressBar.visibility = View.GONE
        Snackbar.make(binding.root, "SAT Results not found", Snackbar.LENGTH_LONG).apply {
            setAction("Retry") {
                detailsViewModel.fetchSchoolAndScores()
            }
            show()
        }
        if (error.data != null) {
            setSchoolDetails(error.data.school)
            if (error.data.score == null) setScoreDetails(error.data.score)
        }
    }

    private fun setScoreDetails(score: HighSchoolSATResults?) {
        binding.mathSatScore.text = getString(R.string.avg_math_score, "0")
        binding.readingSatScore.text = getString(R.string.avg_reading_score, "0")
        binding.writingSatScore.text = getString(R.string.avg_writing_score, "0")
    }

    private fun setSchoolDetails(school: HighSchool) {
        binding.highschoolName.text = school.schoolName
        binding.highschoolDetails.text = school.overviewParagraph
        binding.academicOp.text = "Academic Opportunities"
        binding.academicOpDetails.text =
            school.academicOpportunities1
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    companion object {
        @JvmStatic
        fun newInstance() = DetailsFragment()
    }
}