package com.example.a20230316_vanessacruz_nycschools.ui.schools

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.a20230316_vanessacruz_nycschools.R
import com.example.a20230316_vanessacruz_nycschools.databinding.FragmentSchoolsBinding
import com.example.a20230316_vanessacruz_nycschools.ui.schools.adapter.SchoolsAdapter
import dagger.android.support.DaggerFragment
import javax.inject.Inject

class SchoolsFragment : DaggerFragment() {
    private var _binding: FragmentSchoolsBinding? = null
    private val binding get() = _binding!!

    @Inject

    internal lateinit var viewModelFactory: ViewModelProvider.Factory
    private val schoolsViewModel: SchoolsViewModel by lazy {
        ViewModelProvider(requireActivity(), viewModelFactory)[SchoolsViewModel::class.java]
    }

    private val schoolsAdapter by lazy {
        SchoolsAdapter {
            schoolsViewModel.schoolSelected(it)
            findNavController().navigate(R.id.detail_fragment)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View {
        _binding = FragmentSchoolsBinding.inflate(inflater, container, false)
        val view = binding.root
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupRecyclerView()
        observeViewModel()
        schoolsViewModel.handleIntent(SchoolsListIntent.Init)
    }

    private fun setupRecyclerView() {
        binding.highschoolRecycler.apply {
            adapter = schoolsAdapter
            layoutManager = LinearLayoutManager(requireContext())
        }
    }

    private fun observeViewModel() {
        schoolsViewModel.state.observe(viewLifecycleOwner) {
            when (it) {
                is SchoolsState.Error -> handleError(it)
                is SchoolsState.Loading -> showLoading()
                is SchoolsState.None -> {} // NOOP
                is SchoolsState.SendToDetails -> sendToDetails()
                is SchoolsState.Success -> handleSuccess(it)
            }
        }
    }

    private fun sendToDetails() {
        schoolsViewModel.handleIntent(SchoolsListIntent.ClearState)
    }

    private fun handleSuccess(success: SchoolsState.Success) {
        binding.progressBar.visibility = View.GONE
        schoolsAdapter?.updateItems(success.highSchools)
        binding.highschoolRecycler.visibility = View.VISIBLE
        binding.errorMessage.visibility = View.GONE
        binding.progressBar.visibility = View.GONE
    }

    private fun handleError(error: SchoolsState.Error) {
        binding.progressBar.visibility = View.GONE
        binding.errorMessage.visibility = View.VISIBLE
        binding.highschoolRecycler.visibility = View.GONE
    }

    private fun showLoading() {
        binding.progressBar.visibility = View.VISIBLE
        binding.errorMessage.visibility = View.GONE
        binding.highschoolRecycler.visibility = View.GONE
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    companion object {
        @JvmStatic
        fun newInstance() = SchoolsFragment()
    }

}