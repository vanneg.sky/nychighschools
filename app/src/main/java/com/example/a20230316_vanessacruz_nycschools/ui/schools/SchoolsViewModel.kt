package com.example.a20230316_vanessacruz_nycschools.ui.schools


import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.a20230316_vanessacruz_nycschools.model.HighSchool
import com.example.a20230316_vanessacruz_nycschools.repository.HighSchoolsRepositoryContract
import com.example.a20230316_vanessacruz_nycschools.repository.SATRepositoryContract
import kotlinx.coroutines.launch
import timber.log.Timber
import javax.inject.Inject

class SchoolsViewModel @Inject constructor(
    private val highSchoolsRepository: HighSchoolsRepositoryContract,
    private val satRepo: SATRepositoryContract,
) : ViewModel() {
    private val _state = MutableLiveData<SchoolsState>(SchoolsState.None)
    val state: LiveData<SchoolsState> = _state

    fun handleIntent(intent: SchoolsListIntent) {
        when (intent) {
            SchoolsListIntent.ClearState -> _state.postValue(SchoolsState.None)
            SchoolsListIntent.Init -> handleInit()
            is SchoolsListIntent.SchoolTapped -> schoolTapped(intent)
        }
    }

    fun schoolSelected(school: HighSchool) {
        satRepo.selectedSchool = school
    }

    private fun handleInit() {
        viewModelScope.launch {
            _state.postValue(SchoolsState.Loading)
            try {
                val results = SchoolsState.Success(highSchoolsRepository.fetchHighSchoolsList())
                _state.postValue(results)
            } catch (e: Throwable) {
                e.printStackTrace()
                Timber.e("SATRepository fetchSchoolAndScore() failed")
                _state.postValue(SchoolsState.Error("Error loading schools"))
            }
        }
    }

    private fun schoolTapped(tapped: SchoolsListIntent.SchoolTapped) {
        _state.postValue(SchoolsState.SendToDetails)
    }
}