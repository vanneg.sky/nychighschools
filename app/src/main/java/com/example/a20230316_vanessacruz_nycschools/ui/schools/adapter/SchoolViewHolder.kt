package com.example.a20230316_vanessacruz_nycschools.ui.schools.adapter

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.a20230316_vanessacruz_nycschools.databinding.RowItemHighschoolBinding
import com.example.a20230316_vanessacruz_nycschools.model.HighSchool

class SchoolViewHolder(
    parent: ViewGroup,
    val binding: RowItemHighschoolBinding = RowItemHighschoolBinding.inflate(
        LayoutInflater.from(parent.context), parent, false
    ),
    private val listener: (Int) -> Unit,
) : RecyclerView.ViewHolder(binding.root) {

    @SuppressLint("SetTextI18n")
    fun bindModelToView(model: HighSchool) {
        binding.highschoolName.text = model.schoolName
        binding.highschoolAddress.text = model.primaryAddressLine1 + ", " + model.city
        binding.root.setOnClickListener {
            listener(adapterPosition)
        }
    }
}