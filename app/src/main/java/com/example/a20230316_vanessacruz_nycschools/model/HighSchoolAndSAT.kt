package com.example.a20230316_vanessacruz_nycschools.model

data class HighSchoolAndSAT(
    val school: HighSchool,
    val score: HighSchoolSATResults?,
)