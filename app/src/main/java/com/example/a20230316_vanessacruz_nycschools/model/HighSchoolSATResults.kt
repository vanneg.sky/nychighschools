package com.example.a20230316_vanessacruz_nycschools.model

import com.google.gson.annotations.SerializedName

data class HighSchoolSATResults(
    val dbn: String,
    @SerializedName("school_name")
    val schoolName: String,
    @SerializedName("num_of_sat_test_takers")
    val numberOfSATTakers: String,
    @SerializedName("sat_critical_reading_avg_score")
    val averageCriticalReadingScore: String,
    @SerializedName("sat_math_avg_score")
    val averageMathScore: String,
    @SerializedName("sat_writing_avg_score")
    val averageWritingScore: String,
)