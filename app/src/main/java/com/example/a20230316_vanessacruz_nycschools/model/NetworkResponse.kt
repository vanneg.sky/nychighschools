package com.example.a20230316_vanessacruz_nycschools.model

sealed class NetworkResponse<T> {
    data class Success<T>(val data: T) : NetworkResponse<T>()
    data class Error<T>(val error: Throwable? = null, val data: T? = null) : NetworkResponse<T>()
    class Loading<T> : NetworkResponse<T>()
}